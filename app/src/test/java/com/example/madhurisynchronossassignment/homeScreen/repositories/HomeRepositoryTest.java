package com.example.madhurisynchronossassignment.homeScreen.repositories;

import com.example.madhurisynchronossassignment.homeScreen.interfaces.OnApiRequest;
import com.example.madhurisynchronossassignment.homeScreen.models.WeatherMain;
import com.example.madhurisynchronossassignment.network.NetworkAPI;
import com.example.madhurisynchronossassignment.utils.Constants;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.*;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeRepositoryTest {

    @Rule
    public InstantTaskExecutorRule rule2 = new InstantTaskExecutorRule();

    @Mock
    public NetworkAPI service;

    @Mock
    public OnApiRequest resultCallbackMock;

    @Mock
    Call<WeatherMain> retrofitCallMock;

    @Mock
    public Callback<WeatherMain> retrofitaCallBackMock;

    private HomeRepository homeRepository = null;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        homeRepository = new HomeRepository(service);
    }

    @Test
    public void getWeatherReportsOnSuccess(){
        ArgumentCaptor<Callback<WeatherMain>> retrofitCallbackCaptor = ArgumentCaptor.forClass(retrofitaCallBackMock.getClass());
        when(service.getWeatherReport(0.0, 0.0, Constants.API_KEY)).thenReturn(retrofitCallMock);
        homeRepository.getWeatherReports(0.0,0.0, resultCallbackMock);
        verify(retrofitCallMock).enqueue(retrofitCallbackCaptor.capture());
        retrofitCallbackCaptor.getValue().onResponse(retrofitCallMock, Response.success(getDummyWeatherMain()));
    }

    @Test
    public void getWeatherReportsOnFailureWeatherNull(){
        ArgumentCaptor<Callback<WeatherMain>> retrofitCallbackCaptor = ArgumentCaptor.forClass(retrofitaCallBackMock.getClass());
        when(service.getWeatherReport(0.0, 0.0, Constants.API_KEY)).thenReturn(retrofitCallMock);
        homeRepository.getWeatherReports(0.0,0.0, resultCallbackMock);
        verify(retrofitCallMock).enqueue(retrofitCallbackCaptor.capture());
        retrofitCallbackCaptor.getValue().onResponse(retrofitCallMock, Response.success(getNullWeatherMain()));
    }

    @Test
    public void getWeatherReportsOnFailure(){
        ArgumentCaptor<Callback<WeatherMain>> retrofitCallbackCaptor = ArgumentCaptor.forClass(retrofitaCallBackMock.getClass());
        when(service.getWeatherReport(0.0, 0.0, Constants.API_KEY)).thenReturn(retrofitCallMock);
        homeRepository.getWeatherReports(0.0,0.0, resultCallbackMock);
        verify(retrofitCallMock).enqueue(retrofitCallbackCaptor.capture());
        retrofitCallbackCaptor.getValue().onFailure(retrofitCallMock, getThrowable());
    }

    private Throwable getThrowable(){
        return new Throwable();
    }

    private WeatherMain getDummyWeatherMain(){
        return new WeatherMain();
    }

    private WeatherMain getNullWeatherMain(){
        return null;
    }

}
