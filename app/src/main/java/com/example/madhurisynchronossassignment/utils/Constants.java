package com.example.madhurisynchronossassignment.utils;

public class Constants {

    public static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    public static final String API_KEY = "5ad7218f2e11df834b0eaf3a33a39d2a";

    public static final String BASE_URL = "http://api.openweathermap.org/data/2.5/";
    public static final String IMAGE_URL = "http://openweathermap.org/img/w/";
    public static final String SUCCESS = "success";
    public static final String WEATHER_REPORT = "weather_report";
    public static final String STATUS = "status";
    public static final String FAILURE = "failed";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    public static final String WORK_MANAGER_RESULT = "WORK_MANAGER_RESULT";
}
