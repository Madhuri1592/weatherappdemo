package com.example.madhurisynchronossassignment.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;

import java.text.DecimalFormat;

public class AndroidUtils {

    public static boolean isNetworkAvailable(Context context){
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null)
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
        else return false;
    }

    @SuppressLint("DefaultLocale")
    public static String convertKelvinToCelcius(double tempInKelvin){
        double temp = tempInKelvin - 273.15;
        return String.format("%.2f", temp);
    }
}
