package com.example.madhurisynchronossassignment;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.ExistingPeriodicWorkPolicy;
import androidx.work.NetworkType;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.madhurisynchronossassignment.databinding.ActivityHomeBinding;
import com.example.madhurisynchronossassignment.homeScreen.BaseActivity;
import com.example.madhurisynchronossassignment.homeScreen.PeriodicWorkManager;
import com.example.madhurisynchronossassignment.homeScreen.SessionMaintainer.SessionManager;
import com.example.madhurisynchronossassignment.homeScreen.interfaces.OnLocationReceived;
import com.example.madhurisynchronossassignment.homeScreen.models.WeatherMain;
import com.example.madhurisynchronossassignment.homeScreen.viewmodels.homeViewModel;
import com.example.madhurisynchronossassignment.utils.AndroidUtils;
import com.example.madhurisynchronossassignment.utils.Constants;
import com.example.madhurisynchronossassignment.utils.ViewModelFactory;
import com.squareup.picasso.Picasso;

import java.util.concurrent.TimeUnit;

import static com.example.madhurisynchronossassignment.utils.Constants.LOCATION_PERMISSION_REQUEST_CODE;

public class HomeActivity extends BaseActivity implements OnLocationReceived {

    private static final String TAG = "HomeActivity::";
    private ActivityHomeBinding binding;
    private homeViewModel homeModel;
    private Location mLocation;
    private PeriodicWorkRequest workRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home);

        if(!AndroidUtils.isNetworkAvailable(this)){
            Toast.makeText(this, getResources().getString(R.string.no_network),Toast.LENGTH_SHORT).show();
            return;
        }

        homeModel = new ViewModelProvider(this,
                new ViewModelFactory(this.getApplication()))
                .get(homeViewModel.class);

        if (!checkLocationPermission())
            requestLocationPermission(LOCATION_PERMISSION_REQUEST_CODE, this);
        else
            getCurrentLocation(this);

        initializeReceiver();

        if (SessionManager.getWeatherReport() != null){
            setUI(SessionManager.getWeatherReport());
        }
    }

    private void initializeReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constants.WORK_MANAGER_RESULT);
        registerReceiver(onDataReceived, intentFilter);
    }

    private final BroadcastReceiver onDataReceived = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "onReceive:"  );

            if (intent.getStringExtra(Constants.STATUS).equals(Constants.SUCCESS)) {
                setUI(SessionManager.getWeatherReport());
            }else {
                Toast.makeText(HomeActivity.this,
                        getResources().getString(R.string.api_error),
                        Toast.LENGTH_SHORT).show();
            }
        }
    };


    public void request(){
        Constraints constraint = new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build();

        Data data = createData( mLocation.getLatitude(), mLocation.getLongitude() );

        workRequest = new PeriodicWorkRequest.Builder(PeriodicWorkManager.class,
                2, TimeUnit.HOURS)
                .setConstraints(constraint)
                .setInputData(data)
                .build();

        WorkManager.getInstance(this).enqueueUniquePeriodicWork("weather_app",
                ExistingPeriodicWorkPolicy.KEEP , workRequest);
    }

    public static Data createData(double latitude, double longitude){
      return new Data.Builder()
            .putDouble(Constants.LATITUDE, latitude)
            .putDouble(Constants.LONGITUDE,  longitude)
            .build();
    }

    @Override
    public void onLocationReceived(Location location) {

        mLocation = location;
        Log.d(TAG, "Location::" + location.getLongitude() );
        Log.d(TAG, "Location::" + location.getLatitude() );

        SessionManager.saveLatitude((float) location.getLatitude());
        SessionManager.saveLongitude((float) location.getLongitude());
        request();
    }

    @SuppressLint("SetTextI18n")
    private void setUI(WeatherMain weatherMain) {
        if (weatherMain.getWeatherDetails().size() != 0) {
            binding.textDescription.setText(weatherMain.getWeatherDetails().get(0).getWeatherDescription());
        }

        binding.textFeelsLike.setText(getResources().getString(R.string.feels_like) + AndroidUtils.convertKelvinToCelcius(weatherMain.getMainDetail().getFeelsLike()) + " °C");
        binding.textLocationLat.setText(getResources().getString(R.string.current_lat) + String.valueOf(weatherMain.getLocationDetail().getLat()));
        binding.textLocationLon.setText(getResources().getString(R.string.current_lon) + String.valueOf(weatherMain.getLocationDetail().getLon()));
        binding.textTemperature.setText(AndroidUtils.convertKelvinToCelcius(weatherMain.getMainDetail().getTemp()) + " °C");
        binding.textHumidity.setText(getResources().getString(R.string.humidity) + weatherMain.getMainDetail().getHumidity() + " %");
        binding.textWind.setText(getResources().getString(R.string.wind) + weatherMain.getWindDetail().getSpeed() + " mps");

        String iconUrl = Constants.IMAGE_URL + weatherMain.getWeatherDetails().get(0).getIcon() + ".png";
        Picasso.get().load(iconUrl).into(binding.weatherIcon);
    }

    @Override
    protected void onDestroy() {
        if (onDataReceived!= null)
            unregisterReceiver(onDataReceived);
        super.onDestroy();
    }
}
