package com.example.madhurisynchronossassignment.homeScreen.models;

import com.google.gson.annotations.SerializedName;

public class MainDetail {

    @SerializedName("feels_like")
    private double feelsLike;

    @SerializedName("temp_min")
    private double tempMin;

    @SerializedName("temp_max")
    private double tempMax;

    private double temp;

    private long humidity;

    public MainDetail(double feelsLike, double tempMin, double tempMax, double temp, long humidity) {
        this.feelsLike = feelsLike;
        this.tempMin = tempMin;
        this.tempMax = tempMax;
        this.temp = temp;
        this.humidity = humidity;
    }

    public double getFeelsLike() {
        return feelsLike;
    }

    public double getTempMin() {
        return tempMin;
    }

    public double getTempMax() {
        return tempMax;
    }

    public double getTemp() {
        return temp;
    }

    public long getHumidity() {
        return humidity;
    }
}
