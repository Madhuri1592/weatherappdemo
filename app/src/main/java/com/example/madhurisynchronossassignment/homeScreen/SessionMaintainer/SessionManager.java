package com.example.madhurisynchronossassignment.homeScreen.SessionMaintainer;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;

import com.example.madhurisynchronossassignment.homeScreen.models.WeatherMain;
import com.google.gson.Gson;

public class SessionManager {

    private static final String PREFERENCE_NAME = "weather_app";
    private static final String IS_SCHEDULED = "is_scheduled";
    private static final String LOCATION_VALUE = "location";
    private static final String LATITUDE_VALUE = "latitude_value";
    private static final String LONGITUDE_VALUE = "longitude_value";
    private static final String WEATHER_REPORT = "weather_report";
    private static SharedPreferences sharedPreferences;

    public static SharedPreferences init(Context context){
        sharedPreferences = context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        return sharedPreferences;
    }

    public static void saveLocation(Location location){
        sharedPreferences.edit().putString(LOCATION_VALUE, new Gson().toJson(location)).apply();
    }

    public static void saveLatitude(float latitude){
        sharedPreferences.edit().putFloat(LATITUDE_VALUE, latitude).apply();
    }

    public static float getLatitude(){
       return sharedPreferences.getFloat(LATITUDE_VALUE, 0);
    }

    public static void saveLongitude(float longitude){
        sharedPreferences.edit().putFloat(LONGITUDE_VALUE, longitude).apply();
    }

    public static float getLongitude(){
      return sharedPreferences.getFloat(LONGITUDE_VALUE, 0);
    }

    public static Location getLocation(){
        return new Gson().fromJson(sharedPreferences.getString(LOCATION_VALUE, ""), Location.class);
    }

    public static WeatherMain getWeatherReport() {
        return new Gson().fromJson(sharedPreferences.getString(WEATHER_REPORT ,""), WeatherMain.class);
    }

    public static void saveWeatherReport(String weatherMain) {
       sharedPreferences.edit().putString(WEATHER_REPORT ,weatherMain).apply();
    }
}
