package com.example.madhurisynchronossassignment.homeScreen.interfaces;

import android.location.Location;

public interface OnLocationReceived {

    void onLocationReceived(Location location);
}
