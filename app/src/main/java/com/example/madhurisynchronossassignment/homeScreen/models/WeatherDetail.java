package com.example.madhurisynchronossassignment.homeScreen.models;

import com.google.gson.annotations.SerializedName;

public class WeatherDetail {

    @SerializedName("description")
    private String weatherDescription;

    private String main;

    private String icon;

    public String getWeatherDescription() {
        return weatherDescription;
    }

    public String getMain() {
        return main;
    }

    public String getIcon() {
        return icon;
    }

    public WeatherDetail(String weatherDescription, String main, String icon) {
        this.weatherDescription = weatherDescription;
        this.main = main;
        this.icon = icon;
    }
}
