package com.example.madhurisynchronossassignment.homeScreen;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.work.Data;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.example.madhurisynchronossassignment.homeScreen.SessionMaintainer.SessionManager;
import com.example.madhurisynchronossassignment.homeScreen.interfaces.OnApiRequest;
import com.example.madhurisynchronossassignment.homeScreen.models.WeatherMain;
import com.example.madhurisynchronossassignment.homeScreen.repositories.HomeRepository;
import com.example.madhurisynchronossassignment.network.ApiClient;
import com.example.madhurisynchronossassignment.network.NetworkAPI;
import com.example.madhurisynchronossassignment.utils.Constants;
import com.google.gson.Gson;

public class PeriodicWorkManager extends Worker {
    private Context context;
    public PeriodicWorkManager(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        this.context = context;
    }

    @NonNull
    @Override
    public Result doWork() {

        SessionManager.init(context);

        final Data inputData = getInputData();
        double latitude = inputData.getDouble(Constants.LATITUDE, 0);
        double longitude = inputData.getDouble(Constants.LONGITUDE, 0);

        HomeRepository homeRepository = new HomeRepository(ApiClient.getClient().create(NetworkAPI.class));
        homeRepository.getWeatherReports(latitude, longitude,
                new OnApiRequest() {
                    @Override
                    public void OnSuccess(WeatherMain weatherMain) {
                        SessionManager.saveWeatherReport(new Gson().toJson(weatherMain));

                        Intent intent = new Intent();
                        intent.setAction(Constants.WORK_MANAGER_RESULT);
                        intent.putExtra(Constants.STATUS, Constants.SUCCESS);
                        context.sendBroadcast(intent);

//                        Log.d("WorkManager::", "weatherMain api response:"+ weatherMain.getLocationDetail().getLat());
//                        Log.d("WorkManager::", "weatherMain api response:"+ weatherMain.getLocationDetail().getLon());
//                        Log.d("WorkManager::", "weatherMain api response:"+ weatherMain.getWeatherDetails().get(0).getWeatherDescription());
                    }

                    @Override
                    public void OnError(String message) {
                        Intent intent = new Intent();
                        intent.setAction(Constants.WORK_MANAGER_RESULT);
                        intent.putExtra(Constants.STATUS, Constants.FAILURE);
                        context.sendBroadcast(intent);
                    }
                });

        return Result.success();
    }
}
