package com.example.madhurisynchronossassignment.homeScreen.interfaces;

import com.example.madhurisynchronossassignment.homeScreen.models.WeatherMain;

public interface OnApiRequest {

    void OnSuccess(WeatherMain weatherMain);

    void OnError(String message);

}
