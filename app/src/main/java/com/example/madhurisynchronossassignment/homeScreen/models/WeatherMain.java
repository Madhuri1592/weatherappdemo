package com.example.madhurisynchronossassignment.homeScreen.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class WeatherMain {

    @SerializedName("coord")
    private LocationDetail locationDetail;

    @SerializedName("weather")
    private ArrayList<WeatherDetail> weatherDetails;

    @SerializedName("main")
    private MainDetail mainDetail;

    @SerializedName("wind")
    private WindDetail windDetail;

    private String name;

    public void setWeatherDetails(ArrayList<WeatherDetail> weatherDetails) {
        this.weatherDetails = weatherDetails;
    }

    public void setMainDetail(MainDetail mainDetail) {
        this.mainDetail = mainDetail;
    }

    public void setWindDetail(WindDetail windDetail) {
        this.windDetail = windDetail;
    }

    public ArrayList<WeatherDetail> getWeatherDetails() {
        return weatherDetails;
    }

    public MainDetail getMainDetail() {
        return mainDetail;
    }

    public WindDetail getWindDetail() {
        return windDetail;
    }

    public LocationDetail getLocationDetail() {
        return locationDetail;
    }

    public void setLocationDetail(LocationDetail locationDetail) {
        this.locationDetail = locationDetail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
