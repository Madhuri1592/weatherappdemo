package com.example.madhurisynchronossassignment.homeScreen.models;

import com.google.gson.annotations.SerializedName;

public class WindDetail {

    private float speed;

    @SerializedName("deg")
    private float degree;

    public WindDetail(float speed, float degree) {
        this.speed = speed;
        this.degree = degree;
    }

    public float getSpeed() {
        return speed;
    }

    public float getDegree() {
        return degree;
    }
}
