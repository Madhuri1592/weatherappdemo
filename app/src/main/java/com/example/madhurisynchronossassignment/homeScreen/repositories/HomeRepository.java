package com.example.madhurisynchronossassignment.homeScreen.repositories;


import com.example.madhurisynchronossassignment.homeScreen.interfaces.OnApiRequest;
import com.example.madhurisynchronossassignment.homeScreen.models.WeatherMain;
import com.example.madhurisynchronossassignment.network.NetworkAPI;
import com.example.madhurisynchronossassignment.utils.Constants;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeRepository {

    private final NetworkAPI service;

    public HomeRepository(NetworkAPI serviceParam){
//        service = ApiClient.getClient().create(NetworkAPI.class);
        service = serviceParam;
    }

    public void getWeatherReports(double latitude, double longitude, final OnApiRequest onApiRequest){

        service.getWeatherReport(latitude,
                longitude,
                Constants.API_KEY).enqueue(new Callback<WeatherMain>() {
            @Override
            public void onResponse(Call<WeatherMain> call, Response<WeatherMain> response) {
                WeatherMain weatherMain = response.body();

//                Log.d("Repo", "Success::"  + new Gson().toJson(weatherMain));
                if (weatherMain != null){
                    onApiRequest.OnSuccess(weatherMain);
                }else {
                    onApiRequest.OnError("failure");
                }
            }

            @Override
            public void onFailure(Call<WeatherMain> call, Throwable t) {
//                Log.d("Repo", "onFailure::"  + t.getMessage());
                onApiRequest.OnError("failure");
            }
        });
    }
}
