package com.example.madhurisynchronossassignment.homeScreen.models;

public class WeatherResponse {

    private String message;

    private WeatherMain weatherMain;

    public WeatherResponse(String message, WeatherMain weatherMain) {
        this.message = message;
        this.weatherMain = weatherMain;
    }

    public String getMessage() {
        return message;
    }

    public WeatherMain getWeatherMain() {
        return weatherMain;
    }
}
