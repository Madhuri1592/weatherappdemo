package com.example.madhurisynchronossassignment.homeScreen.viewmodels;

import android.app.Application;
import android.location.Location;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.example.madhurisynchronossassignment.homeScreen.interfaces.OnApiRequest;
import com.example.madhurisynchronossassignment.homeScreen.models.WeatherMain;
import com.example.madhurisynchronossassignment.homeScreen.repositories.HomeRepository;
import com.example.madhurisynchronossassignment.network.ApiClient;
import com.example.madhurisynchronossassignment.network.NetworkAPI;

public class homeViewModel extends AndroidViewModel {
    private HomeRepository homeRepository;

    MutableLiveData<WeatherMain> weatherMainMutableLiveData = new MutableLiveData<>();
    public homeViewModel(@NonNull Application application) {
        super(application);
        homeRepository  = new HomeRepository(ApiClient.getClient().create(NetworkAPI.class));
    }

    public MutableLiveData<WeatherMain> getWeatherReport(Location location){
        homeRepository.getWeatherReports(location.getLatitude(), location.getLongitude(),
                new OnApiRequest() {
                    @Override
                    public void OnSuccess(WeatherMain weatherMain) {
                        weatherMainMutableLiveData.setValue(weatherMain);
                    }

                    @Override
                    public void OnError(String message) {
                        weatherMainMutableLiveData.setValue(new WeatherMain());
                    }
                });
        return weatherMainMutableLiveData;
    }
}
