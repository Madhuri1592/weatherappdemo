package com.example.madhurisynchronossassignment.network;

import com.example.madhurisynchronossassignment.homeScreen.models.WeatherMain;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface NetworkAPI {


    @GET("weather")
    Call<WeatherMain> getWeatherReport(@Query("lat") double lat,
                                       @Query("lon") double lon,
                                       @Query("appid") String appid);
}
