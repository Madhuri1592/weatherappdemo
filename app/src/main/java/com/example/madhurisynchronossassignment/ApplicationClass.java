package com.example.madhurisynchronossassignment;

import android.app.Application;

import com.example.madhurisynchronossassignment.homeScreen.SessionMaintainer.SessionManager;

public class ApplicationClass extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        SessionManager.init(this);
    }
}
