1. Language: Java
2. OS Support version: Lollipop and above
3. Architecture: MVVM
4. APP permissions required: Internet, GPS
5. 3rd Party libraries used: Retrofit, Gson, Picasso

App Flow:
1. User is prompted with Location permission if it is not granted
2. Once permission is granted User current location is fetched and saved
3. Task is scheduled to execute every 2 hours
4. Using saved latitude and longitude weather report is fetched using OpenWeather API
5. Weather response is saved and used for future

App can be enhanced by adding below features:
1. Day & Time selection by the user – Weather report based on day and time selected
2. Location selection - Weather report based on location selected

